/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tccv2;

import java.sql.*;
import java.util.*;

public class TCCV2 {

    public static void main(String[] args) throws Exception {
        try {
            Class.forName("org.firebirdsql.jdbc.FBDriver");
            Properties props = new Properties();
            props.setProperty("user", "SYSDBA");
            props.setProperty("password", "masterkey");
            props.setProperty("encoding", "UNICODE_FSS");
            Connection connection = DriverManager.getConnection(
                    "jdbc:firebirdsql:localhost/3050:/home/nani/Documentos/UTFPR/TCC/base/herbario.gdb",
                    props);
            // do something here
        } catch (Exception e) {
            System.out.println("Não foi possível conectar ao banco: " + e.getMessage());
        }
    }
}
